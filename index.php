<?php
/**
 *
/**/

// Check to see if the mySQL PHP libraries are installed.
if (!function_exists('mysql_connect')) {
    errorHandler("The mySQL libraries are not installed.<br>", true);
    return;
}

if (empty($_SERVER['db_server']) || empty($_SERVER['db_name']) || empty($_SERVER['db_login']) || empty($_SERVER['db_password'])) {
    errorHandler("The required database configuration is missing or incomplete.<br>", true);
}

// Try to connect to the database.  If we can't, notify the user.
$myDBConn = @mysql_connect($_SERVER['db_server'], $_SERVER['db_login'], $_SERVER['db_password'])
    or errorHandler("Can't connect to the database server, please check to make sure the settings are correct.<br>", true);

// Make sure we're attached to the MytTV database.
@mysql_select_db($_SERVER['db_name'], $myDBConn)
    or errorHandler("Unable to switch to the correct database, please check to make sure the settings are correct.<br>", true);

// Query the MythTV database and get a current list of recordings.
$myResults = mysql_query("SELECT title, subtitle, DATE_FORMAT(starttime, '%Y-%m-%d, %l-%i %p') AS starttime, basename FROM recorded ORDER BY title, starttime", $myDBConn);
if (!$myResults) {
    errorHandler("Unable to execute query to obtain current recording list.", true);
}

//  Output the start of a basic HTML page.
print '<html>';
print '<head>';
print '<title>MythiPad</title>';
print '</head>';
print '<body style="background-color:#FFFFFF; ">';

// Start the table listing the currently available recordings and
// output a column header row.
print "<table border=1><tr><td>Show Name</td><td>Episode</td><td>Date</td></tr>";

// Loop through our MythTV recording recordset and output a table row for each one
// including a link to playback the iPad version of the recording.
while ($myRow = mysql_fetch_assoc($myResults)) {
    print '<tr>';
    print '<td>' . $myRow['title'] . '</td>';
    print '<td>' . $myRow['subtitle'] . '</td>';
    print '<td>' . $myRow['starttime'] . '</td>';
    // Depending on version of MythTV used, videos may be .mpg or .ts.
    if (strpos($myRow['basename'], ".mpg") == false) {
        print '<td><a href="' . $_SERVER['video_ipadwebpath'] . str_replace(".ts", ".m4v", $myRow['basename']) . '">Play</a></td>';
    } else {
        print '<td><a href="' . $_SERVER['video_ipadwebpath'] . str_replace(".mpg", ".m4v", $myRow['basename']) . '">Play</a></td>';
    }
    print '</tr>';

    // While we build the display, build the list of current MythTV recording files.  This
    // will be used later to determine which iPad videos we need to delete.
    // Depending on version of MythTV used, videos may be .mpg or .ts.
    if (strpos($myRow['basename'], ".mpg") == false) {
        $mythVideos[] = str_replace(".ts", "", $myRow['basename']);
    } else {
        $mythVideos[] = str_replace(".mpg", "", $myRow['basename']);
    }
}

print '</table>';
print '</body>';
print '</html>';

// TEMPORARILY DISABLED UNTIL DONE WITH OLD VIDEOS.
delete_videos($mythVideos);

// Start of various functions used throughout the program.

// Function:  errorHandler
// Purpose:  Display an error message and optionally terminate execution.
// Parameters:
//    $message:  Text string containing the message to display.
//    $terminate:  Boolean, true to terminate false to continue execution.
function errorHandler($message, $terminate) {
    print $message;
    if ($terminate == true) {
        exit;
    }
} // end errorHandler


// Function:  delete_videos
// Purpose:  Delete transcoded iPad video files if the recording has been deleted
//           in MythTV.
// Parameters:
//    $mythVideos:  Array containing the filenames of existing recordings.  This will be
//                  used to compare transcoded files against to determine which ones need
//                  to be deleted.
function delete_videos($mythVideos) {
    //  Generate the list of iPad videos that currently exist.
    if (is_dir($_SERVER['video_ipaddiskpath'])) {
        if ($dirHandle = opendir($_SERVER['video_ipaddiskpath'])) {
            while (($myFile = readdir($dirHandle)) !== false) {
                if (filetype($_SERVER['video_ipaddiskpath'] . "/" . $myFile) == "file") {
                    $ipadVideos[] = str_replace(".m4v", "", $myFile);
                }
            }
        }
    }

    // Calculate which recordings still exist as iPad videos but the original
    // was deleted in MythTV.
    if ((isset($ipadVideos)) && (count($mythVideos) > 0)) {
        $toDelete = array_diff($ipadVideos, $mythVideos);
        foreach ($toDelete as &$fileToDelete) {
            print "Deleting file: " . $_SERVER['video_ipaddiskpath'] . "/" . $fileToDelete . ".m4v</br>";
            unlink($_SERVER['video_ipaddiskpath'] . "/" . $fileToDelete . ".m4v");
        }
    }
} // end delete_videos
?>
