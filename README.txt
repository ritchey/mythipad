Installation:

1.  The mythtv_ipad_transcode.sh script should go into a bin directory that 
mythtv can access like /usr/bin.  Add it as a 'User Job' to MythTV in the
following manner:

/usr/bin/mythtv_ipad_transcode.sh "%FILE%"

2.  The index.php file should be placed into a directory readable by the web
server.  For Ubuntu a typical location would be:

/var/www/mythipad/index.php

3.  For Ubuntu systems you'll need to 'enable' the new directory.  Copy the
mythipad.conf file into /etc/apache2/sites-available and then create a link
to it from /etc/apache2/sites-enabled.
