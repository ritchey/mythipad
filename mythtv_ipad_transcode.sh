#!/bin/bash 

#User Configuration Items
TARGET_DIR=/mythtv/ipad
SOURCE_DIR=/mythtv

#End User Configuration Items

FILEROOT=`echo $1 | cut -d'.' -f1` 


HandBrakeCLI -i "${SOURCE_DIR}/${1}" -o "${TARGET_DIR}/${FILEROOT}.m4v" --preset=iPad --decomb 
